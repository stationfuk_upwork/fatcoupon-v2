export default class Coupon {
    id: string;

    code: string;

    checked: boolean;
    discount: number;

    constructor(code?: string) {
        this.code = code;
    }
}
