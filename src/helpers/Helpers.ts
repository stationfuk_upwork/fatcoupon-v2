import * as $ from "jquery";
import StoreParser, {StoreType} from "../parser/StoreParser";
import MacysParser from "../parser/stores/MacysParser";
import CartersParser from "../parser/stores/CartersParser";
import AdidasParser from "../parser/stores/AdidasParser";

export default class Helpers {

    private static _generatedIds: any = [];
    static generateId(length: number = 7):string {
        let chars = 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789',
            out = '';
        for(let i=0, clen=chars.length; i<length; i++){
            out += chars.substr(0|Math.random() * clen, 1);
        }
        return Helpers._generatedIds[out] ? Helpers.generateId(length) : (Helpers._generatedIds[out] = out);
    }

    static wait(time: number) {
        return new Promise((resolve) => {
            setTimeout(resolve, time);
        });
    }

    static waitFor (selector: string, root: HTMLElement | HTMLDocument): Promise<HTMLElement> {
        return new Promise((resolve) => {
            let resolved = false;
            let element = $(selector, root).get(0);

            if (element) {
                resolve(element);
            } else {
                let observer = new MutationObserver(function () {
                    if (resolved === false ) {
                        element = $(selector, root).get(0);
                        if (element) {
                            resolve(element);
                            observer.disconnect();
                            resolved = true;
                        }
                    }
                });

                observer.observe(root, {
                    childList: true,
                    subtree: true,
                });
            }
        });
    };

    static randomIntFromInterval(min: number, max: number)
    {
        return Math.floor(Math.random() * (max - min + 1) + min);
    }

    /**
     * Convert StoreType to StoreParser
     * We can't pass StoreParser variable between background and content script so we need this hack
     * @param type
     */
    static getParserFromParseType(type: StoreType): typeof StoreParser {
        switch (type) {
            case StoreType.Macys:
                return MacysParser; // TODO !create apply coupon button!
            case StoreType.Carters:
                return CartersParser;
            case StoreType.Adidas:
                return AdidasParser;
            case StoreType.Gnc:
                return StoreParser;
            case StoreType.Nike:
                return StoreParser;
            default:
                return StoreParser;
        }
    }

    /**
     * "$2,087.09" => 2087.09
     * @param usdString
     */
    static parseUsdString(usdString: string): number {
        let str = usdString.toString();
        try {
            str = str.replace(/([,$ \n])/g, "");
            return parseFloat(str);
        } catch (e) {
        }
        return 0;
    }
}
