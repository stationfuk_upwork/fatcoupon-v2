import StoreParser, {StoreType} from "../parser/StoreParser";
import Coupon from "../types/Coupon";
import Tab = chrome.tabs.Tab;

export enum ExtensionMessageAction {
    Notification,
    ShowSidebar,
    LoadCoupons,
    GetCurrentTab
}

export default class ExtensionMessage<TRequest, TResponse> {
    action: ExtensionMessageAction;

    data: TRequest;
    IResponseType: TResponse;

    constructor(props?: TRequest) {
        this.data = props;
    }

    async sendToBackend(): Promise<TResponse> {
        return new Promise<TResponse>((resolve) => {
            chrome.runtime.sendMessage(this, resolve);
        });
    }
    async sendToTab(tabId: number): Promise<TResponse> {
        return new Promise<TResponse>((resolve) => {
            chrome.tabs.sendMessage(tabId, this, resolve);
        });
    }
}

export interface INotificationMessageRequest {
    notification: Notification;
}
export class NotificationMessage extends ExtensionMessage<INotificationMessageRequest, void> {
    action: ExtensionMessageAction = ExtensionMessageAction.Notification;
}

export class ShowSidebarMessage extends ExtensionMessage<void, void> {
    action: ExtensionMessageAction = ExtensionMessageAction.ShowSidebar;
}

export interface IGetCouponsMessageResponse {
    error: Error
    coupons: Coupon[]
}
export class LoadCouponsMessage extends ExtensionMessage<void, IGetCouponsMessageResponse> {
    action: ExtensionMessageAction = ExtensionMessageAction.LoadCoupons;
}

export class GetCurrentTabMessage extends ExtensionMessage<void, Tab> {
    action: ExtensionMessageAction = ExtensionMessageAction.GetCurrentTab;
}
