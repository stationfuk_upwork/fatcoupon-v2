import {IGetCouponDiscountResponse, StoreType} from "../parser/StoreParser";
import Coupon from "../types/Coupon";
import Helpers from "./Helpers";
import {Store} from "webext-redux";
import {
    setCurCouponIndex,
    showPopup,
    startApplyCoupons, stopApplyCoupons
} from "../reducers/CouponApplierPopup/CouponApplierPopupActions";
import {Logger} from "./Logger";
import {RootState} from "../reducers";
import {setHaveApplied, updateCoupon} from "../reducers/App/AppActions";

export class CouponApplier {

    reduxStore: any;

    storeType: StoreType;

    get parser() {
        return Helpers.getParserFromParseType(this.storeType);
    }
    get reduxState(): RootState {
        return this.reduxStore.getState();
    }
    get coupons() {
        return this.reduxState.app.coupons;
    }
    get curCouponIndex() {
        return this.reduxState.couponApplierPopup.curCouponIndex;
    }

    /**
     * constructor, should be async to wait for store connection
     * @param tabId
     * @param storeType
     */
    static async create(tabId: number, storeType: StoreType): Promise<CouponApplier> {
        let couponApplier = new CouponApplier();
        couponApplier.storeType = storeType;
        couponApplier.reduxStore = new Store({
            portName: `FATCOUPON-${tabId}`
        });
        await couponApplier.reduxStore.ready();
        return couponApplier;
    }

    async startApplyCoupons() {
        Logger.log(`Start applying ${this.coupons.length} coupons...`);

        await this.reduxStore.dispatch(startApplyCoupons());
        await this.reduxStore.dispatch(showPopup());

        await Helpers.wait(1000);
        await this.applyNextCoupon();

        Logger.log(`All coupons applied.`);

        this.stopApplyCoupons();
    }

    async stopApplyCoupons() {
        await this.reduxStore.dispatch(setHaveApplied());
        await this.reduxStore.dispatch(stopApplyCoupons());

        Logger.log(`Stop applying coupons.`);
    }

    applyBestCoupon() {
        Logger.log(`Looking for best coupon...`);
        let bestCoupon = this.getBestDiscountCoupon();
        if (bestCoupon) {
            Logger.log(`Best coupon - ${bestCoupon.code}, applying...`);
            this.parser.applyCoupon(document, bestCoupon);
        } else {
            Logger.log(`No coupons with discount.`);
        }
    }

    /**
     * Return coupon with highest discount or null
     */
    getBestDiscountCoupon() {
        let biggestDiscountCoupon = new Coupon();
        biggestDiscountCoupon.discount = 0;
        this.coupons.forEach((coupon) => {
            if (biggestDiscountCoupon.discount < coupon.discount) {
                biggestDiscountCoupon = coupon;
            }
        });
        return biggestDiscountCoupon.code ? biggestDiscountCoupon : null;
    }

    async applyNextCoupon() {
        // TODO remove after tests
        let debugDelayStart = Date.now();

        Logger.log(`Applying coupon ${this.coupons[this.curCouponIndex].code}...`);

        let result = await this.applyCoupon(this.coupons[this.curCouponIndex]);
        if (result.error || !this.reduxState.couponApplierPopup.isActive) {
            if (result.error) {
                Logger.error(result.error.message);
            }
            return;
        }

        Logger.log(`Coupon ${this.coupons[this.curCouponIndex].code} have discount $${this.coupons[this.curCouponIndex].discount}`);

        // update coupon data in redux store
        await this.reduxStore.dispatch(updateCoupon(result.coupon));

        // TODO remove after tests
        // should delay if applying coupons took less than 3 sec
        // to show interface more clear
        let debugDelay = debugDelayStart + 1000 - Date.now();
        if (debugDelay > 0) {
            await Helpers.wait(debugDelay);
        }

        // if still more coupons - check next one
        if (this.coupons[this.curCouponIndex + 1]) {
            await this.reduxStore.dispatch(setCurCouponIndex(this.curCouponIndex + 1));
            await this.applyNextCoupon();
        }
        // if no more coupons - finish process
    }

    async applyCoupon(coupon: Coupon): Promise<IGetCouponDiscountResponse> {
        return await this.parser.getCouponDiscount(document, coupon);
    }
}

export interface CouponApplierData {
}
