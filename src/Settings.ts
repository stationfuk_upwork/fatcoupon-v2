export default {
    BASE_URI: "http://backend-url.localhost",
    SUPPORTED_WEBSITES: [
        "www.macys.com",
        "www.carters.com",
        "www.adidas.com",
    ]
}
