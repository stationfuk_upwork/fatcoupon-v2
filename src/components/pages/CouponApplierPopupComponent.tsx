import * as React from "react";
import {Redirect, Route, RouteComponentProps, Switch, withRouter} from "react-router";
import Coupon from "../../types/Coupon";
import {RootState} from "../../reducers";
import {setParser, updateCoupon, updateCoupons} from "../../reducers/App/AppActions";
import {hideSidebar, showSidebar} from "../../reducers/Sidebar/SidebarActions";
import {
    hidePopup,
    showPopup,
} from "../../reducers/CouponApplierPopup/CouponApplierPopupActions";
import {connect, ConnectedProps} from "react-redux";
import Helpers from "../../helpers/Helpers";
import {CouponApplier} from "../../helpers/CouponApplier";
import Icon from "../Icon";

import "../../css/coupon-applier-popup.scss";
import {FontAwesomeIcon} from "@fortawesome/react-fontawesome";

const mapState = (state: RootState) => ({
    app: {
        parserType: state.app.parserType,
        coupons: state.app.coupons,
        haveApplied: state.app.haveApplied
    },
    popup: {
        isVisible: state.couponApplierPopup.isVisible,
        isActive: state.couponApplierPopup.isActive,
        isFinished: state.couponApplierPopup.isFinished,
        curCouponIndex: state.couponApplierPopup.curCouponIndex,
    }
});
const mapDispatch = {
    setParser,
    updateCoupons,
    updateCoupon,
    showSidebar,
    hideSidebar,
    showPopup,
    hidePopup,
};
const connector = connect(
    mapState,
    mapDispatch
);

export namespace CouponApplierPopupComponent {
    export type Props = RouteComponentProps<any> & ConnectedProps<typeof connector> & {
        couponApplier: CouponApplier
    }
}

class CouponApplierPopupComponent extends React.Component<CouponApplierPopupComponent.Props, {
    errorMessage: string
    successMessage: string
}> {

    constructor(props: any) {
        super(props);
        this.state = {
            errorMessage: null,
            successMessage: null,
        };
    }

    get parser() {
        return Helpers.getParserFromParseType(this.props.app.parserType);
    }

    startApplying() {
        this.props.couponApplier.startApplyCoupons();
    }

    stopApplying() {
        this.props.couponApplier.stopApplyCoupons();
    }

    applyBestCoupon() {
        this.props.couponApplier.applyBestCoupon();
    }

    async hide() {
        await this.props.hidePopup();
        await this.props.couponApplier.stopApplyCoupons();
    }

    render() {
        let bestDiscountCoupon = this.props.couponApplier.getBestDiscountCoupon();
        let zeroStepProgressAddition = this.props.popup.curCouponIndex === 0 ? 5 : 0;
        let progress = !this.props.popup.isFinished
            ? Math.floor(100 / this.props.app.coupons.length * (this.props.popup.curCouponIndex) + zeroStepProgressAddition)
            : 100;
        return (
            <div id="coupon-applier-popup-container" hidden={!this.props.popup.isVisible}>
                <div className="card border-0">
                    <div className="card-header text-white bg-primary">
                        <span className="float-right cursor-pointer" onClick={this.hide.bind(this)}>
                            <Icon innerClassName="fas fa-times-circle" />
                        </span>
                    </div>
                    <div className="card-body row">
                        <div className="col-md-12">
                            <div className="form-group" hidden={!this.state.errorMessage}>
                                <div className="alert alert-danger">
                                    {this.state.errorMessage}
                                </div>
                            </div>
                            <div className="form-group" hidden={!this.state.successMessage}>
                                <div className="alert alert-success">
                                    {this.state.successMessage}
                                </div>
                            </div>
                        </div>
                        <div className="col-md-12 mb-3">
                            <h4 className="text-center">
                                {
                                    this.props.popup.isActive ? (
                                        `${chrome.runtime.getManifest().name} trying to find the best coupon for you...`
                                    ) : (
                                        bestDiscountCoupon ? (
                                            <span className="text-success">
                                            {chrome.runtime.getManifest().name} found discount ${bestDiscountCoupon.discount} for you!
                                        </span>
                                        ) : (
                                            <span className="text-danger">
                                            {chrome.runtime.getManifest().name} haven't found any working coupons...
                                        </span>
                                        )
                                    )
                                }
                            </h4>
                        </div>
                        <div className="col-md-12">
                            <div className="progress mb-3">
                                <div className={`progress-bar progress-bar-striped ${this.props.popup.isActive ? "progress-bar-animated" : ""}`} role="progressbar"
                                     aria-valuenow={progress} aria-valuemin={0} aria-valuemax={100} style={{width: progress + "%"}}></div>
                            </div>
                        </div>
                        <div className="col-md-12">
                            <p>
                                <span className="mr-2">Checking code:</span>
                                {
                                    this.props.popup.isActive ? (
                                        <span>
                                            <Icon innerClassName="fas fa-spin fa-spinner" outerClassName="mr-1" />
                                            {this.props.app.coupons[this.props.popup.curCouponIndex].code}
                                        </span>
                                    ) : (
                                        <span>-</span>
                                    )
                                }
                                <button className={`btn btn-warning ${bestDiscountCoupon ? "float-right" : ""}`}
                                        onClick={this.startApplying.bind(this)}
                                        hidden={!this.props.popup.isFinished || !this.props.app.haveApplied}
                                >
                                    Reapply all
                                </button>
                            </p>
                            <p>
                                <span className="mr-2">Best code:</span>
                                {
                                    bestDiscountCoupon ? (
                                        <span className="badge badge-primary mt-0">
                                            {bestDiscountCoupon.code}
                                        </span>
                                    ) : (
                                        "no discount"
                                    )
                                }
                            </p>
                        </div>
                        <div className="col-md-12 text-center">
                            <button className="btn btn-danger"
                                    onClick={this.stopApplying.bind(this)}
                                    hidden={this.props.popup.isFinished}
                            >
                                Stop
                            </button>
                            <button className="btn btn-success"
                                    onClick={this.startApplying.bind(this)}
                                    hidden={!this.props.popup.isFinished || this.props.app.haveApplied}
                            >
                                Apply all
                            </button>
                            <button className="btn btn-success btn-lg text-uppercase"
                                    onClick={this.applyBestCoupon.bind(this)}
                                    hidden={!this.props.popup.isFinished || !bestDiscountCoupon}
                            >
                                Get <strong>${bestDiscountCoupon ? bestDiscountCoupon.discount : 0}</strong> discount
                            </button>
                        </div>
                    </div>
                </div>
            </div>
        );
    }
}

export default connector(withRouter(CouponApplierPopupComponent));
