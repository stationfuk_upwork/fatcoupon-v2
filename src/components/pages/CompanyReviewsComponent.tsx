import * as React from "react";
import {Redirect, Route, RouteComponentProps, Switch, withRouter} from "react-router";
import * as $ from "jquery";
import CommentComponent from "./CommentComponent";
import {BrowserStorage} from "../../helpers/BrowserStorage";

export namespace CompanyReviewsComponent {
    export interface Props extends RouteComponentProps<any> {
        onRequest: (endpoint: string, data: object) => any
        errorMessage: string
        successMessage: string
    }
}

class CompanyReviewsComponent extends React.Component<CompanyReviewsComponent.Props, {
    comments: any[]
    employerId: string
    vacancyId: string
    auth: {
        login: string
        hash: string
        id: string
    }
    newComment: string
    commentToReply: number
    busy: boolean
}> {

    constructor(props: any) {
        super(props);
        this.state = {
            comments: [],
            auth: null,
            employerId: null,
            vacancyId: null,
            newComment: "",
            commentToReply: null,
            busy: false
        };
    }

    componentDidMount() {
        this.init();
    }

    async init() {
        let auth = await BrowserStorage.load("auth");
        let pageData = await this.getPageData();
        let response = await this.props.onRequest("vacancies-employers", {
            user_id: auth.id,
            result: pageData
        });
        this.setState({
            employerId: pageData[0].employer_id,
            vacancyId: pageData[0].vacancy_id
        }, async () => {
            if (response.comments) {
                let comments:any[] = [];
                $.each(response.comments, (index, item) => {
                    $.each(item, (employer, empComments) => {
                        comments = $.merge(comments, empComments);
                    });
                });
                $.each(comments, (index, comment) => {
                    if (comment.parent_employer_comment_id === "0") {
                        comment.parent_employer_comment_id = null;
                    }
                });
                this.setState({
                    auth: auth,
                    comments: comments
                });
            }
        });
    }

    async getPageData() {
        const vacancies = {
            $all: $("div.vacancy-serp-item"),
            getVacancy: (item: any) => {
                let $item = $(item);
                let $vacancyLink = $item.find("div.resume-search-item__name a");
                let $employerLink = $item.find("div.vacancy-serp-item__meta-info a.bloko-link.bloko-link_secondary");
                return {
                    employer_name: $employerLink.text().trim(),
                    employer_id: $employerLink.attr("href").match(/[(\d)]+/)[0],
                    vacancy_id: $vacancyLink.attr("href").match(/[(\d)]+/)[0],
                    vacancy_name: $vacancyLink.text().trim()
                }
            }
        };
        let conditions:any = {
            sendVacancies: {
                regExp: /\/search\/vacancy/,
                getPageData: () => {
                    let pageData:any[] = [];
                    $.each(vacancies.$all, (index, item) => {
                        pageData.push(vacancies.getVacancy(item));
                    });
                    return pageData;
                }
            },
            sendCatalog: {
                regExp: /\/catalog\//,
                getPageData: () => {
                    let pageData:any[] = [];
                    $.each(vacancies.$all, (index, item) => {
                        pageData.push(vacancies.getVacancy(item));
                    });
                    return pageData;
                }
            },
            sendVacancy: {
                regExp: /\/vacancy\//,
                getPageData: () => {
                    let vacancy = {
                        $company: $("div.vacancy-company a.vacancy-company-name"),
                        $vacancy: $("div.vacancy-title h1"),
                    };
                    let pageData = {
                        employer_name: vacancy.$company.text().trim(),
                        employer_id: vacancy.$company.attr("href").replace("/employer/", ""),
                        vacancy_name: vacancy.$vacancy.text().trim(),
                        vacancy_id: location.href.match(/[(\d)]+/)[0]
                    };
                    return [pageData];
                }
            },
            sendEmployer: {
                regExp: /\/employer\//,
                getPageData: () => {
                    let employerId = location.href.match(/[(\d)]+/)[0];
                    let pageData = {
                        employer_name: $("div.company-description__name h1").text().trim(),
                        employer_id: employerId,
                        vacancy_name: "company",
                        vacancy_id: employerId
                    };
                    return [pageData];
                }
            }
        };

        for (let type in conditions) {
            if (conditions[type].regExp.test(location.href))
            {
                return conditions[type].getPageData();
            }
        }
        return {
            employer_name: "",
            employer_id: "",
            vacancy_id: "",
            vacancy_name: ""
        }
    }

    onTextChange(e: React.FormEvent<HTMLTextAreaElement>) {
        let newComment = this.state.newComment;
        newComment = e.currentTarget.value;
        this.setState({
            newComment: newComment
        });
    }

    emptyCommentToReply(e: Event) {
        e.preventDefault();
        this.setState({
            commentToReply: null
        });
    }

    selectCommentToReply(id: number) {
        this.setState({
            commentToReply: id
        });
    }

    sendComment() {
        if (this.state.newComment.length > 0) {
            this.setState({
                busy: true
            }, async () => {
                let response = await this.props.onRequest("employer/comment", {
                    parent_employer_comment_id: this.state.commentToReply,
                    vacancy_id: this.state.vacancyId,
                    employer_id: this.state.employerId,
                    user_id: (await BrowserStorage.load("auth")).id,
                    text: this.state.newComment
                });
                if (response.result === "success") {
                    this.state.comments.push(response.comment);
                    this.setState({
                        commentToReply: null,
                        newComment: null,
                        comments: this.state.comments,
                        busy: false
                    });
                }
            });
        }
    }

    render() {
        let comments:any[] = [];
        for (let comment of this.state.comments) {
            if (comment.parent_employer_comment_id) {
                continue;
            }
            comments.push(
                <CommentComponent
                    key={comment.id}
                    editable={comment.user_id === this.state.auth.id}
                    comment={comment}
                    onRequest={this.props.onRequest}
                    selectCommentToReply={this.selectCommentToReply.bind(this)}
                    innerComments={this.state.comments
                        .filter((x: any) => x.parent_employer_comment_id === comment.id)
                        .map((x: any) =>
                            <CommentComponent
                                key={x.id}
                                editable={x.user_id === this.state.auth.id}
                                comment={x}
                                onRequest={this.props.onRequest}
                            />)
                    }
                />
            );
        }
        let selectedCommentToReplyName = "";
        let selectedCommentToReply = this.state.comments.filter((x: any) => x.id === this.state.commentToReply);
        if (selectedCommentToReply.length > 0) {
            let name = selectedCommentToReply[0].name;
            if (!name) {
                let index = selectedCommentToReply[0].login.indexOf('@');
                if (index > -1) {
                    name = selectedCommentToReply[0].login.slice(0, index);
                }
            }
            selectedCommentToReplyName = name;
        }
        return [
                <div key="alertList" className="w-100">
                    <div className="form-group" hidden={!this.props.errorMessage}>
                        <div className="alert alert-danger">
                            {this.props.errorMessage}
                        </div>
                    </div>
                    <div className="form-group" hidden={!this.props.successMessage}>
                        <div className="alert alert-success">
                            {this.props.successMessage}
                        </div>
                    </div>
                </div>,
                <div key="commentList" className="comment-list row mb-3">
                    {
                        comments.length > 0 ? comments : (
                            <div>Комментариев ещё нет. Будьте первым!</div>
                        )
                    }
                </div>,
                <div key="writeComment" className="w-100">
                    <span hidden={!this.state.commentToReply}>
                        <a href="#" className="mr-1" onClick={this.emptyCommentToReply.bind(this)}>
                            <i className="fas fa-times-circle"></i>
                        </a>
                        Ответ на комментарий {selectedCommentToReplyName}:
                    </span>
                    <textarea className="form-control col-md-12 mb-1" maxLength={512} placeholder="Текст отзыва" onChange={this.onTextChange.bind(this)} />
                    <button className="form-control col-md-12 btn btn-success btn-sm" onClick={this.sendComment.bind(this)}>Оставить комментарий</button>
                </div>
        ];
    }
}

export default withRouter(CompanyReviewsComponent);