import * as React from "react";
import {Redirect, Route, RouteComponentProps, Switch, withRouter} from "react-router";
import * as $ from "jquery";
import Settings from "../../Settings";
import Coupon from "../../types/Coupon";
import Helpers from "../../helpers/Helpers";
import {connect, ConnectedProps} from "react-redux";
import {RootState} from "../../reducers";
import {hidePopup, showPopup, startApplyCoupons} from "../../reducers/CouponApplierPopup/CouponApplierPopupActions";
import {hideSidebar, showSidebar} from "../../reducers/Sidebar/SidebarActions";
import {setCouponsLoaded, setParser, updateCoupon, updateCoupons} from "../../reducers/App/AppActions";

import "../../css/sidebar.scss";
import {CouponApplier} from "../../helpers/CouponApplier";
import {FontAwesomeIcon} from "@fortawesome/react-fontawesome";

const mapState = (state: RootState) => ({
    app: {
        tabId: state.app.tabId,
        cartTotal: state.app.cartTotal,
        parser: Helpers.getParserFromParseType(state.app.parserType),
        couponsLoaded: state.app.couponsLoaded,
        coupons: state.app.coupons
    },
    sidebar: {
        isVisible: state.sidebar.isVisible
    }
});
const mapDispatch = {
    setParser,
    setCouponsLoaded,
    updateCoupons,
    updateCoupon,
    showSidebar,
    hideSidebar,
    showPopup
};
const connector = connect(
    mapState,
    mapDispatch
);

export namespace SidebarComponent {
    export type Props = RouteComponentProps<any> & ConnectedProps<typeof connector> & {
        couponApplier: CouponApplier
    }
}

class SidebarComponent extends React.Component<SidebarComponent.Props, {
    errorMessage: string
    successMessage: string
    busy: boolean

    discountedCartCost: number
}> {

    constructor(props: any) {
        super(props);
        this.state = {
            errorMessage: null,
            successMessage: null,
            busy: false,

            discountedCartCost: 0
        };
    }

    componentDidMount() {
        this.init();
    }

    async init() {
        this.props.showSidebar();
    }

    show() {
        this.props.showSidebar();
    }

    hide() {
        this.props.hideSidebar();
    }

    applyAllCoupons() {
        this.props.couponApplier.startApplyCoupons();
    }

    render() {
        if (!this.props.app.couponsLoaded) {
            return <div id="sidebar-container"></div>;
        }
        let bestDiscountCoupon = this.props.couponApplier.getBestDiscountCoupon();
        let coupons = this.props.app.coupons.map((coupon: Coupon, index) => {
            let isbiggestDiscountCoupon =  coupon.checked && bestDiscountCoupon && coupon.discount === bestDiscountCoupon.discount;
            let status = coupon.checked
                ? coupon.discount !== 0
                    ? `$${coupon.discount}`
                    : "no discount"
                : "-";
            return (
                <li key={index}
                    className={`list-group-item ${isbiggestDiscountCoupon ? "text-white bg-success" : ""}`}
                >
                    <span>
                        {coupon.code}
                    </span>
                    <span className="float-right">
                        {status}
                    </span>
                </li>
            )
        });
        return (
            <div id="sidebar-container" className={`container fade ${this.props.sidebar.isVisible ? "show" : ""}`}>
                <div className="card border-light">
                    <div className="card-header d-flex justify-content-between align-items-center">
                        <h5 className="d-inline-block mb-0">{chrome.runtime.getManifest().name} - {this.props.app.parser.storeName}</h5>
                        <div className="d-inline-block float-right">
                            <span className="cursor-pointer" onClick={this.hide.bind(this)}>
                                <i className="fas fa-times-circle"></i>
                            </span>
                        </div>
                    </div>
                    <div className="card-body">
                        <div className="w-100">
                            <div className="col-md-12">
                                <h5 className="text-center font-weight-bold mb-3">
                                    Coupons found!
                                </h5>
                                <ul className="list-group ml-0 mb-3 w-100">
                                    <li className="list-group-item bg-light">
                                        <span>
                                            Cart Total
                                            <span className="ml-2 cursor-pointer" data-toggle="tooltip" data-placement="top" title="Cart Total is price without any coupons applied">
                                                <FontAwesomeIcon icon="question-circle" />
                                            </span>
                                        </span>
                                        <span className="font-weight-bolder float-right">
                                            ${this.props.app.cartTotal}
                                        </span>
                                    </li>
                                    <li className="list-group-item border-success bg-light-green text-success">
                                        <span>
                                            With coupon
                                        </span>
                                        <span className="font-weight-bolder float-right">
                                            {
                                                bestDiscountCoupon ? (
                                                    `$${this.props.app.cartTotal - bestDiscountCoupon.discount}`
                                                ) : (
                                                    "???"
                                                )
                                            }
                                        </span>
                                    </li>
                                </ul>
                                <button className="btn btn-orange font-weight-bold mb-2 w-100" onClick={() => { this.applyAllCoupons(); }}>
                                    Apply {this.props.app.coupons.length} Coupons
                                </button>
                                <div className="w-100 text-center">
                                    <a href="#"
                                       className="text-uppercase text-monospace"
                                       onClick={(e) => {
                                            e.preventDefault();
                                            this.hide();
                                       }}
                                    >
                                        Try later
                                    </a>
                                </div>
                            </div>
                            <hr/>
                            <div className="col-md-12">
                                <p className="w-100 text-center font-weight-bold text-monospace">TEST SECTION</p>
                                <ul className="list-group ml-0 w-100">
                                    {coupons}
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        );
    }
}
export default connector(withRouter(SidebarComponent));
