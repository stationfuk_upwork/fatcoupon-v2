import * as React from "react";
import {match, Route, RouteComponentProps, RouteProps, Switch, withRouter} from "react-router";
import {Logger} from "../helpers/Logger";
import StoreParser, {StoreType} from "../parser/StoreParser";
import { connect, ConnectedProps } from 'react-redux'
import {RootState} from "../reducers";
import SidebarComponent from "./pages/SidebarComponent";
import CouponApplierPopupComponent from "./pages/CouponApplierPopupComponent";
import {setCartTotal, setCouponsLoaded, setParser, updateCoupons} from "../reducers/App/AppActions";
import {CouponApplier} from "../helpers/CouponApplier";
import {LoadCouponsMessage} from "../helpers/ExtensionMessage";
import Helpers from "../helpers/Helpers";

const mapState = (state: RootState) => ({
    app: {
        tabId: state.app.tabId,
        parserType: state.app.parserType,
        parser: Helpers.getParserFromParseType(state.app.parserType),
    }
});
const mapDispatch = {
    setParser,
    updateCoupons,
    setCouponsLoaded,
    setCartTotal
};
const connector = connect(
    mapState,
    mapDispatch
);

export namespace App {
    export type Props = RouteComponentProps & ConnectedProps<typeof connector> & {
    }
}

class App extends React.Component<App.Props, {
    initFinished: boolean
    couponApplier: CouponApplier
}> {

    constructor(props: any) {
        super(props);

        this.state = {
            initFinished: false,
            couponApplier: null
        };
    }

    //notificationSystem: any = null;

    async componentDidMount() {
        this.init();
    }

    async init() {
        Logger.log("app init...");
        //this.setupNotifications();
        let storeParser = StoreParser.detectStore();
        if (storeParser === null) {
            Logger.log("current page isn't store page.");
        }
        Logger.log(`store ${storeParser.storeName} found, waiting for checkout page loaded...`);
        await storeParser.waitForCheckoutPage(document);
        Logger.log("checkout page loaded.");

        await this.props.setParser(storeParser.storeType);
        Logger.log(`parser for ${storeParser.storeName} set.`);

        Logger.log(`loading coupons for ${storeParser.storeName}...`);
        let loadCouponsResult = await new LoadCouponsMessage().sendToBackend();
        if (loadCouponsResult.error) {
            Logger.error(loadCouponsResult.error);
            return;
        }
        await this.props.updateCoupons(loadCouponsResult.coupons);
        await this.props.setCouponsLoaded();
        Logger.log(`coupons loaded.`);

        Logger.log(`looking for cart total...`);
        let getCartTotalResult = await storeParser.getCartTotal(document);
        if (getCartTotalResult.error) {
            Logger.error(getCartTotalResult.error);
            return;
        }
        await this.props.setCartTotal(getCartTotalResult.sum);
        Logger.log(`cart total found - $${getCartTotalResult.sum}.`);

        Logger.log(`creating instanse of CouponApplier...`);
        let couponApplier = await CouponApplier.create(this.props.app.tabId, storeParser.storeType);
        Logger.log(`instanse of CouponApplier created.`);

        this.setState({
            couponApplier: couponApplier,
            initFinished: true
        }, () => {
            Logger.log(`app init finished`);
        });
    }

    // TODO: this code was made for notifications without Redux, need to swtich to package "react-notification-system-redux"
    // setupNotifications() {
    //     this.notificationSystem = this.refs.notificationSystem;
    //     chrome.runtime.onMessage.addListener(async (message: ExtensionMessage<any, any>, sender, sendResponse) => {
    //         switch (message.action) {
    //             case ExtensionMessageAction.Notification:
    //                 let typedMesssage = message as NotificationMessage;
    //                 this.addNotification(typedMesssage.data.notification);
    //                 break;
    //         }
    //     });
    // }

    // addNotification(notification: Notification) {
    //     this.notificationSystem.addNotification(notification);
    // }

    // don't render anything except notifications
    // until init finished
    render() {
        if (!this.state.initFinished) {
            return (
                <div id="app-container"></div>
            );
        }
        return (
            <div id="app-container">
                <SidebarComponent key="sidebar" couponApplier={this.state.couponApplier} />,
                <CouponApplierPopupComponent key="coupon-applier-popup" couponApplier={this.state.couponApplier} />
                {/*<NotificationSystem ref="notificationSystem" />*/}
            </div>
        );
    }
}

export default connector(withRouter(App));
