import * as React from "react";
import {RouteComponentProps, withRouter} from "react-router";
import {BrowserStorage} from "../../helpers/BrowserStorage";
import {Link} from "react-router-dom";

export namespace LoginViewComponent {
    export interface Props extends RouteComponentProps<any> {
        onRequest: (endpoint: string, data: object, onSuccess: (response: any) => void) => void
        openChatPage: () => void
        data: {
            busy: boolean
            successMessage: string
            errorMessage: string
        }
    }
}

class LoginViewComponent extends React.Component<LoginViewComponent.Props, {
    email: string,
    password: string,
}> {

    constructor(props: LoginViewComponent.Props) {
        super(props);
        this.state = {
            email: "",
            password: ""
        };
    }

    onEmailChange(event: React.ChangeEvent<HTMLInputElement>) {
        this.setState({
            email: event.target.value
        });
    }

    onPasswordChange(event: React.ChangeEvent<HTMLInputElement>) {
        this.setState({
            password: event.target.value
        });
    }

    onSubmit(e: Event) {
        e.preventDefault();
        this.props.onRequest("login", {
            login: this.state.email,
            password: this.state.password
        }, async (response: any) => {
            await BrowserStorage.save("auth", {
                login: response.login,
                hash: response.hash,
                id: response.id
            });
            setTimeout(() => {
                this.props.openChatPage();
            }, 500);
        });
        return false;
    }

    googleOAuth(e: Event) {
        e.preventDefault();
    }

    render() {
        return (
            <form className="col-md-12" onSubmit={this.onSubmit.bind(this)}>
                <div className="form-group" hidden={!this.props.data.errorMessage}>
                    <div className="alert alert-danger">
                        {this.props.data.errorMessage}
                    </div>
                </div>
                <div className="form-group" hidden={!this.props.data.successMessage}>
                    <div className="alert alert-success">
                        {this.props.data.successMessage}
                    </div>
                </div>
                <div className="form-group">
                    <input type="email"
                           className="form-control form-control-lg"
                           name="email"
                           value={this.state.email}
                           placeholder="Введи свой email"
                           onChange={(event: React.ChangeEvent<HTMLInputElement>) => {this.onEmailChange(event)}}
                    />
                </div>
                <div className="form-group">
                    <input type="password"
                           className="form-control form-control-lg"
                           name="password"
                           value={this.state.password}
                           placeholder="Введи свой пароль"
                           onChange={(event: React.ChangeEvent<HTMLInputElement>) => {this.onPasswordChange(event)}}
                    />
                </div>
                <div className="form-group">
                    <button className="btn btn-lg btn-primary white-text w-100" disabled={this.props.data.busy} onClick={this.googleOAuth.bind(this)}>
                        {
                            !this.props.data.busy ? (
                                "Google +"
                            ) : (
                                <span>
                                    <i className="fas fa-spinner fa-spin"></i>
                                </span>
                            )
                        }
                    </button>
                </div>
                <div className="form-group">
                    <button type="submit" className="btn btn-lg btn-primary white-text w-100" disabled={this.props.data.busy}>
                        {
                            !this.props.data.busy ? (
                                "Войти"
                            ) : (
                                <span>
                                    <i className="fas fa-spinner fa-spin"></i>
                                </span>
                            )
                        }
                    </button>
                </div>
                <div className="form-group text-center mt-4">
                    <p className="mb-1">
                        <Link to="/auth/recovery">Восстанови пароль</Link>
                    </p>
                    <p className="mb-1">
                        <small>или</small>
                    </p>
                    <p className="mb-0">
                        <Link to="/auth/registration">Зарегистрируйся</Link>,
                    </p>
                    <p>если у тебя нет аккаунта</p>
                </div>
            </form>
        );
    }
}

export default withRouter(LoginViewComponent);
