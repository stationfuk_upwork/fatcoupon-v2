import { applyMiddleware, compose, createStore } from 'redux'
import { createLogger } from 'redux-logger';
import thunk from 'redux-thunk'
const promise = require('redux-promise');

import rootReducer from '../reducers'

export default function configureStore() {
  const logger = createLogger();
  const store = createStore(
    rootReducer,
    {},
    applyMiddleware(
        //promise,
        thunk,
        logger
    )
  );

  /*if (module.hot) {
    // Enable hot module replacement for reducers
    module.hot.accept('../reducers', () => {
      const nextRootReducer = require('../reducers');
      store.replaceReducer(nextRootReducer);
    });
  }*/

  return store;
}
