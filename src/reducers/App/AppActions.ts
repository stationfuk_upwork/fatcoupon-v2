import {
    AppActionTypes,
    SET_CART_TOTAL,
    SET_COUPONS_LOADED,
    SET_HAVE_APPLIED,
    SET_PARSER,
    SET_TAB_ID,
    UPDATE_COUPON,
    UPDATE_COUPONS
} from "./AppTypes";
import Coupon from "../../types/Coupon";
import {StoreType} from "../../parser/StoreParser";

export function setParser(type: StoreType): AppActionTypes {
    return {
        type: SET_PARSER,
        payload: type
    }
}

export function setCouponsLoaded(): AppActionTypes {
    return {
        type: SET_COUPONS_LOADED
    }
}

export function setTabId(tabId: number): AppActionTypes {
    return {
        type: SET_TAB_ID,
        payload: tabId
    }
}

export function setCartTotal(cartTotal: number): AppActionTypes {
    return {
        type: SET_CART_TOTAL,
        payload: cartTotal
    }
}

export function setHaveApplied(): AppActionTypes {
    return {
        type: SET_HAVE_APPLIED
    }
}

export function updateCoupons(coupons: Coupon[]): AppActionTypes {
    return {
        type: UPDATE_COUPONS,
        payload: coupons
    }
}

export function updateCoupon(coupon: Coupon): AppActionTypes {
    return {
        type: UPDATE_COUPON,
        payload: coupon
    }
}
