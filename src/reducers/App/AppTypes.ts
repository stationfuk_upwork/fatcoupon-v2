import Coupon from "../../types/Coupon";
import StoreParser, {StoreType} from "../../parser/StoreParser";

export interface AppState {
    tabId: number

    parserType: StoreType

    cartTotal: number
    haveApplied: boolean

    couponsLoaded: boolean
    coupons: Coupon[]

    user: object
}

export const SET_PARSER = 'SET_PARSER';
export const SET_COUPONS_LOADED = 'SET_COUPONS_LOADED';
export const SET_TAB_ID = 'SET_TAB_ID';
export const SET_CART_TOTAL = 'SET_CART_TOTAL';
export const SET_HAVE_APPLIED = 'SET_HAVE_APPLIED';
export const UPDATE_COUPONS = 'UPDATE_COUPONS';
export const UPDATE_COUPON = 'UPDATE_COUPON';

interface SetParserAction {
    type: typeof SET_PARSER
    payload: StoreType
}
interface SetCouponsLoadedAction {
    type: typeof SET_COUPONS_LOADED
}
interface SetTabIdAction {
    type: typeof SET_TAB_ID
    payload: number
}
interface SetCartTotalAction {
    type: typeof SET_CART_TOTAL
    payload: number
}
interface SetHaveAppliedAction {
    type: typeof SET_HAVE_APPLIED
}
interface UpdateCouponsAction {
    type: typeof UPDATE_COUPONS
    payload: Coupon[]
}
interface UpdateCouponAction {
    type: typeof UPDATE_COUPON
    payload: Coupon
}

export type AppActionTypes =
    SetHaveAppliedAction |
    SetCartTotalAction |
    SetTabIdAction |
    SetCouponsLoadedAction |
    SetParserAction |
    UpdateCouponsAction |
    UpdateCouponAction;
