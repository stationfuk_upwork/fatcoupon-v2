import {HIDE_SIDEBAR, SHOW_SIDEBAR, SidebarActionTypes, SidebarState} from "./SidebarTypes";

const initialState: SidebarState = {
  isVisible: false
};

export default function sidebarReducer(
    state = initialState,
    action: SidebarActionTypes
): SidebarState {
  switch (action.type) {
    case SHOW_SIDEBAR:
      return {
        ...state,
        isVisible: true
      };
    case HIDE_SIDEBAR:
      return {
        ...state,
        isVisible: false
      };
    default:
      return state;
  }
}
