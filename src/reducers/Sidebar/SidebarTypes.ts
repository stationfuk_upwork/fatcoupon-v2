export interface SidebarState {
    isVisible: boolean
}

export const SHOW_SIDEBAR = 'SHOW_SIDEBAR';
export const HIDE_SIDEBAR = 'HIDE_SIDEBAR';

interface ShowSidebarAction {
    type: typeof SHOW_SIDEBAR
}
interface HideSidebarAction {
    type: typeof HIDE_SIDEBAR
}

export type SidebarActionTypes = ShowSidebarAction | HideSidebarAction;
