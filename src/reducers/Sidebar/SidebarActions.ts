import {SidebarActionTypes, HIDE_SIDEBAR, SHOW_SIDEBAR} from "./SidebarTypes";

export function showSidebar(): SidebarActionTypes {
    return {
        type: SHOW_SIDEBAR
    }
}

export function hideSidebar(): SidebarActionTypes {
    return {
        type: HIDE_SIDEBAR
    }
}
