import {
  AppActionTypes,
  AppState,
  SET_CART_TOTAL,
  SET_COUPONS_LOADED,
  SET_HAVE_APPLIED,
  SET_PARSER,
  SET_TAB_ID,
  UPDATE_COUPON,
  UPDATE_COUPONS
} from "./AppTypes";
import StoreParser, {StoreType} from "../../parser/StoreParser";

const initialState: AppState = {
  tabId: 0,

  parserType: null,

  cartTotal: 0,
  haveApplied: false,

  couponsLoaded: false,
  coupons: [],

  user: {}
};

export default function appReducer(
    state = initialState,
    action: AppActionTypes
): AppState {
  switch (action.type) {
    case SET_PARSER:
      switch (action.payload) {
        case StoreType.Carters:
        case StoreType.Macys:
        case StoreType.Adidas:
          return {
            ...state,
            parserType: action.payload
          };
        default:
          return {
            ...state,
            parserType: StoreType.NotIdentified
          };
      }
    case SET_TAB_ID:
      return {
        ...state,
        tabId: action.payload
      };
    case SET_CART_TOTAL:
      return {
        ...state,
        cartTotal: action.payload
      };
    case SET_COUPONS_LOADED:
      return {
        ...state,
        couponsLoaded: true
      };
    case SET_HAVE_APPLIED:
      return {
        ...state,
        haveApplied: true
      };
    case UPDATE_COUPONS:
      return {
        ...state,
        coupons: action.payload
      };
    case UPDATE_COUPON:
      state.coupons.forEach((coupon) => {
        if (coupon.code === action.payload.code) {
          coupon.checked = action.payload.checked;
          coupon.discount = action.payload.discount;
        }
      });
      return {
        ...state,
        coupons: state.coupons
      };
    default:
      return state;
  }
}
