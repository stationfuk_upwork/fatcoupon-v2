import {
    CouponApplierPopupActionTypes,
    HIDE_POPUP,
    SET_ACTIVE,
    SET_CUR_COUPON_INDEX,
    SHOW_POPUP,
    START_APPLY_COUPONS,
    STOP_APPLY_COUPONS
} from "./CouponApplierPopupTypes";

export function showPopup(): CouponApplierPopupActionTypes {
    return {
        type: SHOW_POPUP
    }
}

export function hidePopup(): CouponApplierPopupActionTypes {
    return {
        type: HIDE_POPUP
    }
}

export function startApplyCoupons(): CouponApplierPopupActionTypes {
    return {
        type: START_APPLY_COUPONS
    }
}

export function stopApplyCoupons(): CouponApplierPopupActionTypes {
    return {
        type: STOP_APPLY_COUPONS
    }
}

export function setCurCouponIndex(index: number): CouponApplierPopupActionTypes {
    return {
        type: SET_CUR_COUPON_INDEX,
        payload: index
    }
}

export function setActive(active: boolean): CouponApplierPopupActionTypes {
    return {
        type: SET_ACTIVE,
        payload: active
    }
}

export function setFinished(finished: boolean): CouponApplierPopupActionTypes {
    return {
        type: SET_ACTIVE,
        payload: finished
    }
}
