export interface CouponApplierPopupState {
    isVisible: boolean
    isActive: boolean
    isFinished: boolean
    curCouponIndex: number
}

export const SHOW_POPUP = 'SHOW_POPUP';
export const HIDE_POPUP = 'HIDE_POPUP';
export const START_APPLY_COUPONS = 'START_APPLY_COUPONS';
export const STOP_APPLY_COUPONS = 'STOP_APPLY_COUPONS';
export const SET_CUR_COUPON_INDEX = 'SET_CUR_COUPON_INDEX';
export const SET_ACTIVE = 'SET_ACTIVE';
export const SET_FINISHED = 'SET_FINISHED';

interface ShowPopupAction {
    type: typeof SHOW_POPUP
}
interface HidePopupAction {
    type: typeof HIDE_POPUP
}
interface StartApplyCouponsAction {
    type: typeof START_APPLY_COUPONS
}
interface StopApplyCouponsAction {
    type: typeof STOP_APPLY_COUPONS
}
interface SetCurCouponIndexAction {
    type: typeof SET_CUR_COUPON_INDEX
    payload: number
}
interface SetActiveAction {
    type: typeof SET_ACTIVE
    payload: boolean
}
interface SetFinishedAction {
    type: typeof SET_FINISHED
    payload: boolean
}

export type CouponApplierPopupActionTypes =
    ShowPopupAction |
    HidePopupAction |
    StartApplyCouponsAction |
    StopApplyCouponsAction |
    SetCurCouponIndexAction |
    SetActiveAction |
    SetFinishedAction;
