import {
  CouponApplierPopupActionTypes,
  CouponApplierPopupState,
  HIDE_POPUP, SET_ACTIVE,
  SET_CUR_COUPON_INDEX, SET_FINISHED,
  SHOW_POPUP,
  START_APPLY_COUPONS,
  STOP_APPLY_COUPONS
} from "./CouponApplierPopupTypes";

const initialState: CouponApplierPopupState = {
  isVisible: false,
  isActive: false,
  isFinished: false,
  curCouponIndex: -1
};

export default function couponApplierPopupReducer(
    state = initialState,
    action: CouponApplierPopupActionTypes
): CouponApplierPopupState {
  switch (action.type) {
    case SHOW_POPUP:
      return {
        ...state,
        isVisible: true
      };
    case HIDE_POPUP:
      return {
        ...state,
        isVisible: false
      };
    case START_APPLY_COUPONS:
      return {
        ...state,
        isActive: true,
        isFinished: false,
        curCouponIndex: 0
      };
    case STOP_APPLY_COUPONS:
      return {
        ...state,
        isActive: false,
        isFinished: true
      };
    case SET_CUR_COUPON_INDEX:
      return {
        ...state,
        curCouponIndex: action.payload
      };
    case SET_ACTIVE:
      return {
        ...state,
        isActive: action.payload
      };
    case SET_FINISHED:
      return {
        ...state,
        isFinished: action.payload
      };
    default:
      return state;
  }
}
