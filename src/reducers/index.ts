import { combineReducers } from 'redux';
import appReducer from './App/AppReducer';
import couponApplierPopupReducer from "./CouponApplierPopup/CouponApplierPopupReducer";
import sidebarReducer from "./Sidebar/SidebarReducer";

const rootReducer = combineReducers({
  app: appReducer,
  sidebar: sidebarReducer,
  couponApplierPopup: couponApplierPopupReducer
});

export default rootReducer;
export type RootState = ReturnType<typeof rootReducer>
