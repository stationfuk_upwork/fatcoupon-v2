import {Logger} from "./helpers/Logger";
import ExtensionMessage, {
	ExtensionMessageAction,
	LoadCouponsMessage,
	ShowSidebarMessage
} from "./helpers/ExtensionMessage";
import Settings from "./Settings";
import Coupon from "./types/Coupon";
import {StoreType} from "./parser/StoreParser";
import {wrapStore} from 'webext-redux';
import configureStore from "./store/configureStore";
import {RootState} from "./reducers";
import WebNavigationEventFilter = chrome.webNavigation.WebNavigationEventFilter;

export default class BackgroundController {

	reduxStores: any[];

    constructor () {
        this.init();
    }

    init() {
        Logger.log("app started");

        this.reduxStores = [];

        this.initOnInstall();
        this.initOnUpdated();
        this.initScriptInjectRules();
        this.initMessages();
        this.initPageAction();
    }

	/**
	 * Init content script inject rules
	 */
	initScriptInjectRules() {
		let injectWebsitesFilter:WebNavigationEventFilter = {
			url: []
		};
		Settings.SUPPORTED_WEBSITES.forEach((host) => {
			injectWebsitesFilter.url.push({
				hostSuffix: host
			});
		});
		chrome.webNavigation.onCommitted.addListener(function(details) {
			chrome.tabs.executeScript(details.tabId, {
				file: "js/content.js"
			});
		}, injectWebsitesFilter);
	}

	/**
	 * Init ext icon click event listener
	 */
    initPageAction() {
		chrome.pageAction.onClicked.addListener(async (tab) => {
			await new ShowSidebarMessage().sendToTab(tab.id);
		});
	}

	/**
	 * Init messages listener
	 */
	initMessages() {
		chrome.runtime.onMessage.addListener(async (message: ExtensionMessage<any, any>, sender, sendResponse) => {
			switch (message.action) {
				case ExtensionMessageAction.GetCurrentTab:
					this.initStore(sender.tab.id);
					sendResponse(sender.tab);
					break;
				case ExtensionMessageAction.LoadCoupons:
					let typedMessage = message as LoadCouponsMessage;
					let result:(typeof typedMessage.IResponseType) = {
						error: null,
						coupons: []
					};
					switch (this.getStoreState(sender.tab.id).app.parserType) {
						case StoreType.Carters:
							result.coupons = [
								new Coupon("CART0120"),
								new Coupon("BUNDLE20"),
								new Coupon("CART6325"),
								new Coupon("ODVU8186"),
								new Coupon("BBC20JAN05"),
								new Coupon("WNDU9671"),
								new Coupon("CART6310"),
								new Coupon("CART6293"),
								new Coupon("CART5805"),
								new Coupon("CART6286"),
								new Coupon("CART6309"),
							];
							break;
						case StoreType.Macys:
							result.coupons = [
								new Coupon("HOUR48"),
								new Coupon("JOY"),
								new Coupon("COACH30"),
								new Coupon("HOME"),
								new Coupon("SUN50"),
							];
							break;
						case StoreType.Adidas:
							result.coupons = [
								new Coupon("uni15cspd-YWOQ-XHM5-DUWK-6XBRG"),
								new Coupon("CYBER"),
							];
							break;
					}
					sendResponse(result);
					break;
			}
			return false;
		});
	}

	/**
	 * Init onInstall event listener
	 */
	initOnInstall() {
		chrome.runtime.onInstalled.addListener(() => {
			// ********************************************
			// Setup icon to be active on selected websites
			// ********************************************
			let pageActionWebsitesFilter:any[] = [];
			Settings.SUPPORTED_WEBSITES.forEach((host) => {
				pageActionWebsitesFilter.push(
					new chrome.declarativeContent.PageStateMatcher({
						pageUrl: {
							hostEquals: host
						}
					})
				);
			});
			chrome.declarativeContent.onPageChanged.removeRules(undefined, () => {
				chrome.declarativeContent.onPageChanged.addRules([{
					conditions: pageActionWebsitesFilter,
					actions: [
						new chrome.declarativeContent.ShowPageAction()
					]
				}]);
			});
		});
	}

	/**
	 * Init onUpdated event listener
	 */
	initOnUpdated() {
		/*chrome.tabs.onUpdated.addListener((tabId, changeInfo, tab) => {
			//if (changeInfo.url) {
				this.initStore(tabId);
			//}
		});*/
	}

	/**
	 * Init Redux store and make it available from content script
	 */
	initStore(tabId: number) {
		//if (!this.reduxStores[tabId]) {
			Logger.log(`Init Redux store for tab#${tabId}`);
			this.reduxStores[tabId] = configureStore();
			wrapStore(this.reduxStores[tabId], {
				portName: `FATCOUPON-${tabId}`
			});
		//}
	}

	getStoreState(tabId: number): RootState {
		return this.reduxStores[tabId].getState();
	}
}

(<any>window).controller = new BackgroundController() as any;
