import * as React from "react";
import * as ReactDOM from "react-dom";
import * as $ from "jquery";
import Helpers from "./helpers/Helpers";
import AppRouter from "./components/AppRouter";
import { Provider } from 'react-redux'
import {Store} from 'webext-redux';

import "./css/content.scss"
import "./lib/fontawesome.min"
import {GetCurrentTabMessage} from "./helpers/ExtensionMessage";
import {setTabId} from "./reducers/App/AppActions";

setup();

async function setup() {
    // get current tabId
    let tab = await new GetCurrentTabMessage().sendToBackend();

    // connect to background redux store
    let store = new Store({
        portName: `FATCOUPON-${tab.id}`
    });

    // wait for the store to connect to the background page
    await store.ready();

    // save tabId inside redux store
    await store.dispatch(setTabId(tab.id));

    // wait while body tag appear (because we will append root tag after body)
    await Helpers.waitFor("body", document);
    // append root to <html>, not <body> to reduce dependencies
    $("<fatcoupon-root><fatcoupon-content></fatcoupon-content></fatcoupon-root>").insertAfter(document.body);

    // wait for content tag
    let contentElem = await Helpers.waitFor("fatcoupon-content", document);

    // render app inside root
    ReactDOM.render(
        <Provider store={store}>
            <AppRouter />
        </Provider>
        , contentElem);
}

