import StoreParser, {IGetCartTotalResponse, IGetCouponDiscountResponse, StoreType} from "../StoreParser";
import Helpers from "../../helpers/Helpers";
import Coupon from "../../types/Coupon";
import * as $ from "jquery";
import Settings from "../../Settings";
import ErrorTextStatus = JQuery.Ajax.ErrorTextStatus;
import Cookies from "../../helpers/Cookies";
import configureStore from "../../store/configureStore";
import {Logger} from "../../helpers/Logger";

export default class CartersParser extends StoreParser {
    static storeType = StoreType.Carters;
    static storeName = "Carter's";

    static data:any = {
        checkoutPageSelector: "#cartSummary",
        promoFormSelector: "#promo-code-card form",
        secureKeySelector: "[name='dwfrm_cart_securekey']",
    };

    static waitForCheckoutPage(root: HTMLElement | HTMLDocument): Promise<HTMLElement> {
        return Helpers.waitFor(this.data.checkoutPageSelector, root);
    };

    static async getCouponDiscount(document: HTMLDocument, coupon: Coupon): Promise<IGetCouponDiscountResponse> {
        return new Promise<IGetCouponDiscountResponse>(async (resolve) => {
            let result:IGetCouponDiscountResponse = {
                error: null,
                coupon: coupon
            };
            try {
                let promoFormElem = await Helpers.waitFor(this.data.promoFormSelector, document) as HTMLFormElement;
                let secureKeyElem = await Helpers.waitFor(this.data.secureKeySelector, promoFormElem) as HTMLInputElement;
                $.ajax({
                    url : $(promoFormElem).attr("action"),
                    type : 'POST',
                    data: {
                        dwfrm_cart_securekey: secureKeyElem.value,
                        dwfrm_cart_coupons_i0_deleteCoupon: "Remove Code"
                    },
                    success : async (htmlResponse) => {
                        $.ajax({
                            url : $(promoFormElem).attr("action"),
                            type : 'POST',
                            data: {
                                dwfrm_cart_couponCode: coupon.code,
                                dwfrm_cart_securekey: secureKeyElem.value,
                                dwfrm_cart_addCoupon: "dwfrm_cart_addCoupon"
                            },
                            success : async (htmlResponse) => {
                                result.coupon.discount = 0;
                                let couponCardElem = await Helpers.waitFor("#promo-code-card", htmlResponse);
                                Helpers.waitFor(".cart-coupon-error", couponCardElem).then(() => {
                                    // coupon not valid
                                    result.coupon.checked = true;
                                    resolve(result);
                                });
                                Helpers.waitFor(".coupon-status-msg > .wcc-alert--confirm", couponCardElem).then((alertElem) => {
                                    result.coupon.checked = true;
                                    result.coupon.discount = Helpers.parseUsdString($(alertElem).find("span > strong").text());
                                    resolve(result);
                                });
                            },
                            error : async (jqXHR: any, textStatus: ErrorTextStatus, errorThrown: string) => {
                                result.error = new Error("Get cart total request error:" + errorThrown);
                                resolve(result);
                            }
                        });
                    },
                    error : async (jqXHR: any, textStatus: ErrorTextStatus, errorThrown: string) => {
                        result.error = new Error("Get cart total request error:" + errorThrown);
                        resolve(result);
                    }
                });
            } catch (error) {
                result.error = new Error("Get cart total error: " + error);
                resolve(result);
            }
        });
    };

    static async getCartTotal(document: HTMLDocument): Promise<IGetCartTotalResponse> {
        return new Promise<IGetCartTotalResponse>(async (resolve) => {
            let result:IGetCartTotalResponse = {
                error: null,
                sum: 0
            };
            try {
                let promoFormElem = await Helpers.waitFor(this.data.promoFormSelector, document) as HTMLFormElement;
                let secureKeyElem = await Helpers.waitFor(this.data.secureKeySelector, promoFormElem) as HTMLInputElement;
                $.ajax({
                    url : $(promoFormElem).attr("action"),
                    type : 'POST',
                    data: {
                        dwfrm_cart_securekey: secureKeyElem.value,
                        dwfrm_cart_coupons_i0_deleteCoupon: "Remove Code"
                    },
                    success : async (htmlResponse) => {
                        $.ajax({
                            url : $(promoFormElem).attr("action"),
                            type : 'POST',
                            data: {
                                dwfrm_cart_couponCode: "NON_EXISTING_CODE",
                                dwfrm_cart_securekey: secureKeyElem.value,
                                dwfrm_cart_addCoupon: "dwfrm_cart_addCoupon"
                            },
                            success : async (htmlResponse) => {
                                let orderTotalElem = await Helpers.waitFor("#orderTotal", htmlResponse);
                                result.sum = Helpers.parseUsdString($(orderTotalElem).text());
                                // @ts-ignore
                                resolve(result);
                            },
                            error : async (jqXHR: any, textStatus: ErrorTextStatus, errorThrown: string) => {
                                result.error = new Error("Get cart total request error:" + errorThrown);
                                resolve(result);
                            }
                        });
                    },
                    error : async (jqXHR: any, textStatus: ErrorTextStatus, errorThrown: string) => {
                        result.error = new Error("Get cart total request error:" + errorThrown);
                        resolve(result);
                    }
                });
            } catch (error) {
                result.error = new Error("Get cart total error: " + error);
                resolve(result);
            }
        });
    };

    static async applyCoupon(document: HTMLDocument, coupon: Coupon) {
        // Carter's applying and checking scheme is same
        let result = await this.getCouponDiscount(document, coupon);
        Logger.log("TODO: switch off cart total checking after applying best code (it erasing best code discount!)");
        location.reload();
    };
}
