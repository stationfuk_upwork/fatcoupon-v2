import StoreParser, {IGetCartTotalResponse, IGetCouponDiscountResponse, StoreType} from "../StoreParser";
import Helpers from "../../helpers/Helpers";
import Coupon from "../../types/Coupon";
import * as $ from "jquery";
import Settings from "../../Settings";
import ErrorTextStatus = JQuery.Ajax.ErrorTextStatus;
import Cookies from "../../helpers/Cookies";
import configureStore from "../../store/configureStore";
import {Logger} from "../../helpers/Logger";

export default class MacysParser extends StoreParser {
    static storeType = StoreType.Macys;
    static storeName = "Macy's";

    static data:any = {
        checkoutPageSelector: "#bag-main-container",
        cartTotalSelector: "#cx-at-GRAND_TOTAL-value",
    };

    static waitForCheckoutPage(root: HTMLElement | HTMLDocument): Promise<HTMLElement> {
        return Helpers.waitFor(this.data.checkoutPageSelector, root);
    };

    static async getCouponDiscount(document: HTMLDocument, coupon: Coupon): Promise<IGetCouponDiscountResponse> {
        return new Promise<IGetCouponDiscountResponse>((resolve) => {
            let result:IGetCouponDiscountResponse = {
                error: null,
                coupon: coupon
            };
            try {
                let siteContextElem = $("header", document);
                let currencyCode = "USD";
                if (siteContextElem.length > 0) {
                    currencyCode = JSON.parse(siteContextElem.attr("data-context")).currencyCode;
                }
                let bagId = Cookies.readDocumentCookie(document, "macys_bagguid");
                $.ajax({
                    url : `https://www.macys.com/my-bag/${bagId}/promo?currencyCode=${currencyCode}&promoCode=${coupon.code}`,
                    type : 'PUT',
                    dataType:'JSON',
                    success : async (data) => {
                        let discount = 0;
                        let offers = data.bag.sections.bagPromotions.manual;
                        if (offers.length > 0 && offers[0].discount) {
                            discount = offers[0].discount.values[0].value;
                        }
                        result.coupon.checked = true;
                        result.coupon.discount = discount;
                        resolve(result);
                    },
                    error : async (jqXHR: any, textStatus: ErrorTextStatus, errorThrown: string) => {
                        result.error = new Error("Apply coupon request error:" + errorThrown);
                        resolve(result);
                    }
                });
            } catch (error) {
                result.error = new Error("Apply coupon error: " + error);
                resolve(result);
            }
        });
    };

    static async getCartTotal(document: HTMLDocument): Promise<IGetCartTotalResponse> {
        return new Promise<IGetCartTotalResponse>((resolve) => {
            // parsing from page not available
            // let result:IGetCartTotalResponse = {
            //     error: null,
            //     sum: 0
            // };
            // try {
            //     let cartTotalElem = await Helpers.waitFor(this.data.cartTotalSelector, document);
            //     result.sum = Helpers.parseUsdString($(cartTotalElem).text());
            //     resolve(result);
            // } catch (error) {
            //     result.error = new Error("Get cart total error: " + error);
            //     resolve(result);
            // }
            let result:IGetCartTotalResponse = {
                error: null,
                sum: 0
            };
            try {
                let siteContextElem = $("header", document);
                let currencyCode = "USD";
                if (siteContextElem.length > 0) {
                    currencyCode = JSON.parse(siteContextElem.attr("data-context")).currencyCode;
                }
                let bagId = Cookies.readDocumentCookie(document, "macys_bagguid");
                $.ajax({
                    url : `https://www.macys.com/my-bag/${bagId}/promo?currencyCode=${currencyCode}&promoCode=NON_EXISTING_CODE`,
                    type : 'PUT',
                    dataType:'JSON',
                    success : async (data) => {
                        result.sum = data.bag.sections.summary.price[3].values[0].value;
                        resolve(result);
                    },
                    error : async (jqXHR: any, textStatus: ErrorTextStatus, errorThrown: string) => {
                        result.error = new Error("Apply coupon request error:" + errorThrown);
                        resolve(result);
                    }
                });
            } catch (error) {
                result.error = new Error("Apply coupon error: " + error);
                resolve(result);
            }
        });
    };

    static async applyCoupon(document: HTMLDocument, coupon: Coupon) {
        // Macy's applying and checking scheme is same
        let result = await this.getCouponDiscount(document, coupon);
        Logger.log("TODO: switch off cart total checking after applying best code (it erasing best code discount!)");
        location.reload();
    };
}
