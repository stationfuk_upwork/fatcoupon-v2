import {IGetCouponsMessageResponse, LoadCouponsMessage} from "../helpers/ExtensionMessage";
import Coupon from "../types/Coupon";
import configureStore from "../store/configureStore";

export enum StoreType {
    NotIdentified,
    Carters,
    Macys,
    Adidas,
    Gnc,
    Nike
}

export interface IGetCouponDiscountResponse {
    error: Error
    coupon: Coupon
}
export interface IGetCartTotalResponse {
    error: Error
    sum: number
}
export default class StoreParser {
    static storeType: StoreType;
    static storeName: string;

    static coupons: any;
    static data: any;

    static waitForCheckoutPage(root: HTMLElement | HTMLDocument): Promise<HTMLElement> {
        return new Promise(null);
    };

    static async getCartTotal(root: HTMLElement | HTMLDocument): Promise<IGetCartTotalResponse> {
        return new Promise(null);
    };

    static getCouponDiscount(document: HTMLDocument, coupon: Coupon): Promise<IGetCouponDiscountResponse> {
        return new Promise(null);
    };

    static applyCoupon(document: HTMLDocument, coupon: Coupon) {
        return new Promise(null);
    };

    /**
     * Returns derived type of StoreParser (like MacysParser)
     */
    static detectStore(): typeof StoreParser {
        switch (document.location.host) {
            case "www.macys.com":
                return require("./stores/MacysParser").default;
            case "www.carters.com":
                return require("./stores/CartersParser").default;
            case "www.adidas.com":
                return require("./stores/AdidasParser").default;
        }
        return null;
    };
}
